#!/bin/python3

import os
import sys

def scale(direction, s):
    return (direction[0] * s, direction[1] * s)

def add(a, b):
    return (a[0] + b[0], a[1] + b[1])

def performSearch(searchArgs, keys):
    
    directions = [(-1, 0), (-1, -1), (0, -1), (1, -1), (1, 0), (1, 1), (0, 1), (-1, -1)]
    
    for sh in searchArgs:
        wordlen = len(sh)
        testword = ""
        # print(sh[0])
        if sh[0] not in keys:
            break
        for candidates in keys[sh[0]]:
            for direction in directions:
                word = []
                for i in range(wordlen):
                    x, y = add(candidates, scale(direction, i))
                    if x < 0 or x > cols -1:
                        break
                    if y < 0 or y > rows -1:
                        break
                    word.append(matrix[x][y])

                testword = ''.join(word)
                if(sh == testword):
                    print(f'{sh} {candidates[0]}:{candidates[1]} {x}:{y}')
                    break    
    pass

if __name__ == '__main__':

    gridSize = input()
    # print(gridSize)

    xloc = gridSize.find('x')

    rows = int(gridSize[0:xloc])
    cols = int(gridSize[xloc + 1 : len(gridSize)])

    matrix = []
    for i in range(rows):
        matrix.append(list(map(str, input().rstrip().split())))

    # print(matrix)
    data = sys.stdin.readlines()

    searchArgs = (list(map(lambda x: x.rstrip(), data)))

    keys = {}

    for i in range(rows):
        for j in range(cols):
            if matrix[i][j] not in keys:
                keys[matrix[i][j]] = []
            keys[matrix[i][j]].append((i,j))

    performSearch(searchArgs, keys)

